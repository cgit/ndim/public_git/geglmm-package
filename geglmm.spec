Name:		geglmm
Version:	0.0.22
Release:	2%{?dist}
Summary:	C++ mm-style bindings for GEGL

Group:		System Environment/Libraries
License:	LGPLv2+ and LGPLv3+ and GPLv2+ and GPLv3+
# Is there a better URL?
URL:		http://ftp.gnome.org/pub/gnome/sources/geglmm/
Source0:	http://ftp.gnome.org/pub/gnome/sources/geglmm/0.0/%{name}-%{version}.tar.bz2
BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildRequires:	gegl-devel >= %{version}


%description
C++ mm-style bindings for GEGL (Generic Graphics Library).


%package	devel
Summary:	Development files for %{name}
Group:		Development/Libraries
Requires:	%{name} = %{version}-%{release}


%description	devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%setup -q


%build
%configure --disable-static
make %{?_smp_mflags}


%install
rm -rf "%{buildroot}"
make install DESTDIR="%{buildroot}"
find "%{buildroot}" -name '*.la' -exec rm -f {} ';'


%clean
rm -rf "%{buildroot}"


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc README AUTHORS NEWS ChangeLog
%doc COPYING COPYING.LESSER
%{_libdir}/libgeglmm.so.2
%{_libdir}/libgeglmm.so.2.0.0


%files devel
%defattr(-,root,root,-)
%doc
%{_includedir}/geglmm-1.0/geglmm.h
%{_includedir}/geglmm-1.0/geglmm/buffer.h
%{_includedir}/geglmm-1.0/geglmm/color.h
%{_includedir}/geglmm-1.0/geglmm/curve.h
%{_includedir}/geglmm-1.0/geglmm/init.h
%{_includedir}/geglmm-1.0/geglmm/node.h
%{_includedir}/geglmm-1.0/geglmm/operation.h
%{_includedir}/geglmm-1.0/geglmm/path.h
%{_includedir}/geglmm-1.0/geglmm/private/buffer_p.h
%{_includedir}/geglmm-1.0/geglmm/private/color_p.h
%{_includedir}/geglmm-1.0/geglmm/private/curve_p.h
%{_includedir}/geglmm-1.0/geglmm/private/node_p.h
%{_includedir}/geglmm-1.0/geglmm/private/operation_p.h
%{_includedir}/geglmm-1.0/geglmm/private/path_p.h
%{_includedir}/geglmm-1.0/geglmm/private/processor_p.h
%{_includedir}/geglmm-1.0/geglmm/private/rectangle_p.h
%{_includedir}/geglmm-1.0/geglmm/processor.h
%{_includedir}/geglmm-1.0/geglmm/rectangle.h
%{_includedir}/geglmm-1.0/geglmm/wrap_init.h
%{_libdir}/libgeglmm.so
%{_libdir}/geglmm-1.0/include/geglmmconfig.h
%{_libdir}/geglmm-1.0/proc/m4/convert.m4
%{_libdir}/geglmm-1.0/proc/m4/convert_libgeglmm.m4
%{_libdir}/pkgconfig/geglmm.pc


%changelog
* Fri Feb 13 2009 Hans Ulrich Niedermann <hun@n-dimensional.de> - 0.0.22-2
- use mktemp for buildroot
- consistently use macros instead of env vars
- ship license files

* Thu Feb 12 2009 Hans Ulrich Niedermann <hun@n-dimensional.de> - 0.0.22-1
- Initial package

